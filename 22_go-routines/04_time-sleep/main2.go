package main

import (
	"fmt"
	"time"
)

func main() {
	fmt.Println("Hello1")
	time.Sleep(3 * time.Second)
	fmt.Println("Hello2")
	time.Sleep(3 * time.Second)
}

// Just playing with time package. It's pretty cool.
